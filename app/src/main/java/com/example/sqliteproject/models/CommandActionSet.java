package com.example.sqliteproject.models;

import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;

public class CommandActionSet implements Parcelable {

    public String id;
    public String actionType;
    public String actionName;
    public String actionTime = "0.5";
    public int commandType;
    public Icon commandIconsArrayList;
    public DetectedIcons detectedIcons = new DetectedIcons();
    public String description = "";
    public String HEADER = "A55A";
    public String dataLength = "0F00";
    public String command = "";
    public String rawCommand = "";
    public int pressure = 50;





    public CommandActionSet(){

    }

    protected CommandActionSet(Parcel in) {
        id = in.readString();
        commandType = in.readInt();
        actionType = in.readString();
        actionName = in.readString();
        actionTime = in.readString();
        commandIconsArrayList = in.readParcelable(Icon.class.getClassLoader());
        detectedIcons = in.readParcelable(DetectedIcons.class.getClassLoader());
        description = in.readString();
        HEADER = in.readString();
        dataLength = in.readString();
        command = in.readString();
        rawCommand = in.readString();
        pressure = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(commandType);
        dest.writeString(actionType);
        dest.writeString(actionName);
        dest.writeString(actionTime);
        dest.writeParcelable(commandIconsArrayList, flags);
        dest.writeParcelable(detectedIcons, flags);
        dest.writeString(description);
        dest.writeString(HEADER);
        dest.writeString(dataLength);
        dest.writeString(command);
        dest.writeString(rawCommand);
        dest.writeInt(pressure);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CommandActionSet> CREATOR = new Creator<CommandActionSet>() {
        @Override
        public CommandActionSet createFromParcel(Parcel in) {
            return new CommandActionSet(in);
        }

        @Override
        public CommandActionSet[] newArray(int size) {
            return new CommandActionSet[size];
        }
    };
}
