package com.example.sqliteproject.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class Commands implements Parcelable , Comparable {

    public String id;
    public String actionType;
    public boolean secondTime = false;
    public String actionName;
    public String actionTime = "2";
    public CommandActionSet defaultAction;
    public ArrayList<CommandActionSet> commandActionSetsList = new ArrayList<>();
    public DetectedIcons detectedIcons;
    public String description = "";
    public String HEADER = "A55A";
    public String dataLength = "0F00";
    public String command = "";
    public boolean isSaved = false;
    public String FOOTER = "AFFA";
    public String rawCommand = "";
    public int commandType;
    public String bluRemoteCommand = "";
    public boolean isEditable = false;
    public String appendTime;
    public String scriptId = "";
    public int pressure =50;





    public Commands(){

    }
    public Commands(String appendTime){
        this.appendTime = appendTime;
    }

    protected Commands(Parcel in) {
        id = in.readString();
        actionType = in.readString();
        secondTime = in.readByte() != 0;
        actionName = in.readString();
        actionTime = in.readString();
        defaultAction = in.readParcelable(CommandActionSet.class.getClassLoader());
        commandActionSetsList = in.createTypedArrayList(CommandActionSet.CREATOR);
        detectedIcons = in.readParcelable(DetectedIcons.class.getClassLoader());
        description = in.readString();
        HEADER = in.readString();
        dataLength = in.readString();
        command = in.readString();
        isSaved = in.readByte() != 0;
        FOOTER = in.readString();
        rawCommand = in.readString();
        commandType = in.readInt();
        bluRemoteCommand = in.readString();
        isEditable = in.readByte() != 0;
        appendTime = in.readString();
        scriptId = in.readString();
        pressure = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(actionType);
        dest.writeByte((byte) (secondTime ? 1 : 0));
        dest.writeString(actionName);
        dest.writeString(actionTime);
        dest.writeParcelable(defaultAction, flags);
        dest.writeTypedList(commandActionSetsList);
        dest.writeParcelable(detectedIcons, flags);
        dest.writeString(description);
        dest.writeString(HEADER);
        dest.writeString(dataLength);
        dest.writeString(command);
        dest.writeByte((byte) (isSaved ? 1 : 0));
        dest.writeString(FOOTER);
        dest.writeString(rawCommand);
        dest.writeInt(commandType);
        dest.writeString(bluRemoteCommand);
        dest.writeByte((byte) (isEditable ? 1 : 0));
        dest.writeString(appendTime);
        dest.writeString(scriptId);
        dest.writeInt(pressure);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public static final Creator<Commands> CREATOR = new Creator<Commands>() {
        @Override
        public Commands createFromParcel(Parcel in) {
            return new Commands(in);
        }

        @Override
        public Commands[] newArray(int size) {
            return new Commands[size];
        }
    };

    @Override
    public int compareTo(Object o) {
        int detectedIconY = ((Commands) o).detectedIcons.y;
        return this.detectedIcons.y-detectedIconY;

    }

    public Commands getSerialNumber(){

        String prepend = HEADER + dataLength+appendTime;

        Commands commands = new Commands();
        commands.rawCommand = appendTime+"0B00";
        Log.d(TAG, "getSerialNumber: "+commands.rawCommand);
//        commands.command = prepend + "0B00" + bleUtils.convertCrcToLittleEndian(commands.rawCommand) + FOOTER;
        commands.description = "= Get Serial Number";

        return commands;
    }

    public Commands getSoftwareNumber(){

        String prepend = HEADER + dataLength+appendTime;

        Commands commands = new Commands();
        commands.rawCommand = appendTime+"0C";
//        commands.command = prepend + "0C" + bleUtils.convertCrcToLittleEndian(commands.rawCommand) + FOOTER;
        commands.description = "= Get Software Number";

        return commands;
    }


    public Commands getHardwareNumber(){

        String prepend = HEADER + dataLength+appendTime;

        Commands commands = new Commands();
        commands.rawCommand = appendTime+"0D";
//        commands.command = prepend + "0D" + bleUtils.convertCrcToLittleEndian(commands.rawCommand) + FOOTER;
        commands.description = "= Get Hardware Number";

        return commands;
    }


}
