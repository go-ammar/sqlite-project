package com.example.sqliteproject.models;

import android.os.Parcel;
import android.os.Parcelable;

public class DetectedIcons implements Parcelable {

    public String title;
    public String url;
    public int x;
    public int y;
    public int width;
    public int height;
    public byte isTested= 0;

    public DetectedIcons(){

    }

    protected DetectedIcons(Parcel in) {
        title = in.readString();
        url = in.readString();
        x = in.readInt();
        y = in.readInt();
        width = in.readInt();
        height = in.readInt();
        isTested = in.readByte();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(url);
        dest.writeInt(x);
        dest.writeInt(y);
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeByte(isTested);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetectedIcons> CREATOR = new Creator<DetectedIcons>() {
        @Override
        public DetectedIcons createFromParcel(Parcel in) {
            return new DetectedIcons(in);
        }

        @Override
        public DetectedIcons[] newArray(int size) {
            return new DetectedIcons[size];
        }
    };
}
