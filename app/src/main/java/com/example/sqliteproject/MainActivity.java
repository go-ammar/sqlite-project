package com.example.sqliteproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    DatabaseHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionViews();
    }

    private void actionViews() {

        mHelper = new DatabaseHelper(this);

//        for (int i = 0; i < 10; i++) {
//            Log.d(TAG, "actionViews: " + i);
//            mHelper.insertButtonData(String.valueOf(i), String.valueOf(i), 1.0, 1.0, 2.0, 2.0);
//        }

//        mHelper
        mHelper.updateButtonData("3", "0", 1.0, 1.0, 2.0, 2.0);

        Log.d(TAG, "actionViews: buttons " + mHelper.getButtons());
        for (int i =0; i<mHelper.getButtons().size(); i++){
            Log.d(TAG, "actionViews: "+i+" "+mHelper.getButtons().get(i).url);
        }
        Log.d(TAG, "actionViews: commands " + mHelper.getCommands());
        Log.d(TAG, "actionViews: commands action " + mHelper.getCommandAction());
//        Log.d(TAG, "actionViews: x " + mHelper.getX());

    }


}