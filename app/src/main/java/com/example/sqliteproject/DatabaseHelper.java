package com.example.sqliteproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.sqliteproject.models.CommandActionSet;
import com.example.sqliteproject.models.Commands;
import com.example.sqliteproject.models.DetectedIcons;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String COL_NAME_BUTTON = "_NAME";
    public static final String COL_ICON = "_ICONURL";
    public static final String COL_X = "_X";
    public static final String COL_Y = "_Y";
    public static final String COL_WIDTH = "_WIDTH";
    public static final String COL_HEIGHT = "_HEIGHT";
    public static final String COL_COMMAND_NAME = "_NAME";
    public static final String COL_BUTTON_NAME = "_BUTTON_NAME";
    public static final String COL_ID = "_ID";
    public static final String COL_TIME = "_TIME";
    public static final String COL_PRESSURE = "_PRESSURE";
    public static final String COL_TYPE = "_TYPE";
    public static final String COL_COMMAND_ID = "_COMMAND_ID";
    public static final String COL_DESCRIPTION = "_COMMAND_ACTION_DESCRIPTION";
    public static final String COL_COMMAND_ACTION_ID = "_COMMAND_ACTION_ID";
    public static final String COL_HEADER = "_COMMAND_ACTION_HEADER";
    public static final String COL_DATALENGTH = "_COMMAND_ACTION_DATALENGTH";
    public static final String COL_COMMAND_ACTION_NAME = "_COMMAND_ACTION_NAME";

    public static final String COL_COMMAND_RAW_COMMAND = "_COMMAND_RAW_ACTION";
    public static final String COL_SCRIPT_ID = "_COMMAND_SCRIPT_ID";
    public static final String COL_BLUEREMOTE_COMMAND = "_BLUEREMOTE_COMMAND";

    private static final String TAG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "user.db";

    private static final String button_table = "button_table";
    public static final String CREATE_TABLE_BUTTON = "CREATE TABLE IF NOT EXISTS " + button_table + " ("
            + COL_NAME_BUTTON + " TEXT PRIMARY KEY, "
            + COL_ICON + " TEXT, "
            + COL_X + " INTEGER, "
            + COL_Y + " INTEGER, "
            + COL_WIDTH + " INTEGER, "
            + COL_HEIGHT + " INTEGER"
            + ");";

    private static final String command_table = "command_table";
    public static final String CREATE_TABLE_COMMAND = "CREATE TABLE IF NOT EXISTS " + command_table + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_COMMAND_NAME + " TEXT, "
            + COL_BUTTON_NAME + " TEXT, "
            + COL_COMMAND_RAW_COMMAND + " TEXT, "
            + COL_SCRIPT_ID + " TEXT, "
            + COL_BLUEREMOTE_COMMAND + " TEXT, "
            + "FOREIGN KEY(" + COL_BUTTON_NAME + ") REFERENCES " + button_table + "(" + COL_NAME_BUTTON + ")"
            + ");";

    private static final String commandAction_table = "commandAction_table";
    public static final String CREATE_TABLE_COMMAND_ACTION = "CREATE TABLE IF NOT EXISTS " + commandAction_table + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_COMMAND_ACTION_NAME + " TEXT, "
            + COL_BUTTON_NAME + " TEXT, "
            + COL_TIME + " TEXT, "
            + COL_TYPE + " TEXT, "
            + COL_PRESSURE + " INTEGER, "
            + COL_DESCRIPTION + " TEXT, "
            + COL_HEADER + " TEXT, "
            + COL_DATALENGTH + " TEXT, "
            + "FOREIGN KEY(" + COL_BUTTON_NAME + ") REFERENCES " + button_table + "(" + COL_NAME_BUTTON + ")"
            + ");";

    private static final String x_table = "x_table";
    public static final String CREATE_TABLE_X = "CREATE TABLE IF NOT EXISTS " + x_table + " ("
            + COL_COMMAND_ID + " INTEGER , "
            + COL_COMMAND_ACTION_ID + " INTEGER, "
            + "PRIMARY KEY (" + COL_COMMAND_ID + "," + COL_COMMAND_ACTION_ID + ")"
            + ");";
    SQLiteDatabase db;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//        db.execSQL("DROP TABLE user.button_table;");
//        db.execSQL("DROP TABLE user.command_table;");
//        db.execSQL("DROP TABLE user.commandAction_table;");
//        db.execSQL("DROP TABLE user.x_table;");
        db.execSQL(CREATE_TABLE_BUTTON);
        db.execSQL(CREATE_TABLE_COMMAND);
        db.execSQL(CREATE_TABLE_COMMAND_ACTION);
        db.execSQL(CREATE_TABLE_X);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion < 2) {
            upgradeVersion2(db);

        }
        if (oldVersion < 3) {
//            upgradeVersion3(db);

        }
        if (oldVersion < 4) {
//            upgradeVersion4(db);

        }
    }

    private void upgradeVersion2(SQLiteDatabase db) {
        String newCol = "sa";
        //add column newCol
        db.execSQL("ALTER TABLE " + button_table + " ADD COLUMN " + newCol + " INTEGER DEFAULT 0;");
        //rename table
        db.execSQL("ALTER TABLE " + button_table + " RENAME TO " + newCol + ";");
        //rename column to newCol
        db.execSQL("ALTER TABLE " + button_table + " RENAME COLUMN " + " oldCol " + " TO " + newCol + ";");
//        db.execSQL("ALTER TABLE " + button_table + " DROP COLUMN " + newCol + " INTEGER DEFAULT 0;");
        onCreate(db);

    }

    public void insertButtonData(String name, String iconUrl, double x, double y, double height, double width) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME_BUTTON, name);
        contentValues.put(COL_ICON, iconUrl);
        contentValues.put(COL_X, x);
        contentValues.put(COL_Y, y);
        contentValues.put(COL_WIDTH, width);
        contentValues.put(COL_HEIGHT, height);
        db.insert(button_table, null, contentValues);

    }

    public void insertCommandData(String name, String buttonName, String rawCommand, String scriptId, String blueRemoteCommand) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_COMMAND_NAME, name);
        contentValues.put(COL_BUTTON_NAME, buttonName);
        contentValues.put(COL_COMMAND_RAW_COMMAND, rawCommand);
        contentValues.put(COL_SCRIPT_ID, scriptId);
        contentValues.put(COL_BLUEREMOTE_COMMAND, blueRemoteCommand);

        db.insert(command_table, null, contentValues);

    }

    public void insertCA(String buttonName, String time, int type, int pressure, String description, String header, String dataLength) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_BUTTON_NAME, buttonName);
        contentValues.put(COL_TIME, time);
        contentValues.put(COL_TYPE, type);
        contentValues.put(COL_PRESSURE, pressure);
        contentValues.put(COL_DESCRIPTION, description);
        contentValues.put(COL_HEADER, header);
        contentValues.put(COL_DATALENGTH, dataLength);
        db.insert(commandAction_table, null, contentValues);

    }

    public void insertX(String x) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_COMMAND_ID, x);
        contentValues.put(COL_COMMAND_ACTION_ID, x);

        db.insert(x_table, null, contentValues);

    }

    public void updateButtonData(String name, String iconUrl, double x, double y, double height, double width) {
        ContentValues cv = new ContentValues();
        cv.put(COL_ICON, iconUrl);
        cv.put(COL_X, x);
        cv.put(COL_Y, y);
        cv.put(COL_WIDTH, width);
        cv.put(COL_HEIGHT, height);
        db.update(button_table, cv, COL_NAME_BUTTON + "=" + name, null);
        Log.d(TAG, "updateButtonData: ");
    }

    public void updateCommandData(String id, String name, String buttonName, String rawCommand, String scriptId, String blueRemoteCommand) {
        ContentValues cv = new ContentValues();
        cv.put(COL_COMMAND_NAME, name);
        cv.put(COL_BUTTON_NAME, buttonName);
        cv.put(COL_COMMAND_RAW_COMMAND, rawCommand);
        cv.put(COL_SCRIPT_ID, scriptId);
        cv.put(COL_BLUEREMOTE_COMMAND, blueRemoteCommand);

        db.update(command_table, cv, COL_ID + "=" + id, null);
        Log.d(TAG, "updateCommandData: ");
    }

    public void updateCommandActionData(String id, String buttonName, String time, int type, int pressure, String description, String header, String dataLength) {
        ContentValues cv = new ContentValues();
        cv.put(COL_BUTTON_NAME, buttonName);
        cv.put(COL_TIME, time);
        cv.put(COL_TYPE, type);
        cv.put(COL_PRESSURE, pressure);
        cv.put(COL_DESCRIPTION, description);
        cv.put(COL_HEADER, "A55A");
        cv.put(COL_DATALENGTH, "0F00");

        db.update(commandAction_table, cv, COL_ID + "=" + id, null);
        Log.d(TAG, "updateButtonData: ");
    }

    public void deleteButtonData(String buttonName) {
        db.delete(button_table, COL_NAME_BUTTON + "=" + buttonName, null);
    }

    public void deleteCommandData(String commandId) {
        db.delete(command_table, COL_ID + "=", new String[]{commandId});
    }

    public void deleteCommandActionData(String commandActionId) {
        db.delete(commandAction_table, COL_ID + "=", new String[]{commandActionId});
    }

    public ArrayList<DetectedIcons> getButtons() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = new String[]{COL_NAME_BUTTON, COL_ICON, COL_X, COL_Y, COL_WIDTH, COL_HEIGHT};
        Cursor cursor = db.query(button_table, columns, null, null,
                null, null, null);

        ArrayList<DetectedIcons> detectedIcons = new ArrayList<>();

        int colName = cursor.getColumnIndex(COL_NAME_BUTTON);
        int icon = cursor.getColumnIndex(COL_ICON);
        int colX = cursor.getColumnIndex(COL_X);
        int colY = cursor.getColumnIndex(COL_Y);
        int width = cursor.getColumnIndex(COL_WIDTH);
        int height = cursor.getColumnIndex(COL_HEIGHT);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

            DetectedIcons icons = new DetectedIcons();
            icons.height = cursor.getInt(height);
            icons.width = cursor.getInt(width);
            icons.x = cursor.getInt(colX);
            icons.y = cursor.getInt(colY);
            icons.url = cursor.getString(icon);
            icons.title = cursor.getString(colName);

            detectedIcons.add(icons);

        }

        cursor.close();
        return detectedIcons;
    }

    public ArrayList<Commands> getCommands() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = new String[]{COL_ID, COL_COMMAND_NAME, COL_BUTTON_NAME, COL_COMMAND_RAW_COMMAND, COL_SCRIPT_ID,
                COL_BLUEREMOTE_COMMAND};
        Cursor cursor = db.query(command_table, columns, null, null,
                null, null, null);

        ArrayList<Commands> commandsArrayList = new ArrayList<>();
        int commandId = cursor.getColumnIndex(COL_ID);
        int commandName = cursor.getColumnIndex(COL_COMMAND_NAME);
        int buttonName = cursor.getColumnIndex(COL_BUTTON_NAME);
        int rawCommand = cursor.getColumnIndex(COL_COMMAND_RAW_COMMAND);
        int scriptId = cursor.getColumnIndex(COL_SCRIPT_ID);
        int blueRemoteCommand = cursor.getColumnIndex(COL_BLUEREMOTE_COMMAND);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Commands commands = new Commands();
            commands.id = cursor.getString(commandId);
            commands.actionName = cursor.getString(commandName);
            commands.detectedIcons = getButtonFromName(cursor.getString(buttonName));
            commands.rawCommand = cursor.getColumnName(rawCommand);
            commands.scriptId = cursor.getColumnName(scriptId);
            commands.bluRemoteCommand = cursor.getColumnName(blueRemoteCommand);

            commandsArrayList.add(commands);

        }
        cursor.close();
        return commandsArrayList;
    }

    public DetectedIcons getButtonFromName(String buttonName) {
        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = new String[]{COL_NAME_BUTTON, COL_ICON, COL_X, COL_Y, COL_WIDTH, COL_HEIGHT};
//        Cursor cursor = db.query(button_table, columns, null, null,
//                null, null, null);

        Cursor c = db.rawQuery("SELECT * FROM " + button_table + " WHERE " + COL_NAME_BUTTON + " = ?", new String[]{buttonName});

        int colName = c.getColumnIndex(COL_NAME_BUTTON);
        int icon = c.getColumnIndex(COL_ICON);
        int colX = c.getColumnIndex(COL_X);
        int colY = c.getColumnIndex(COL_Y);
        int width = c.getColumnIndex(COL_WIDTH);
        int height = c.getColumnIndex(COL_HEIGHT);

        if (c.moveToFirst()) {
            DetectedIcons icons = new DetectedIcons();

            icons.height = c.getInt(height);
            icons.width = c.getInt(width);
            icons.x = c.getInt(colX);
            icons.y = c.getInt(colY);
            icons.url = c.getString(icon);
            icons.title = c.getString(colName);

            c.close();
            return icons;
        }

//        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//
//            DetectedIcons icons = new DetectedIcons();
//
//            if (buttonName.equals(cursor.getString(colName))) {
//                icons.height = cursor.getInt(height);
//                icons.width = cursor.getInt(width);
//                icons.x = cursor.getInt(colX);
//                icons.y = cursor.getInt(colY);
//                icons.url = cursor.getString(icon);
//                icons.title = cursor.getString(colName);
//
//                return icons;
//            }
//
//        }

        c.close();
        return null;


    }

    public CommandActionSet getCommandActionFromName(String CommandName) {
        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = new String[]{COL_NAME_BUTTON, COL_ICON, COL_X, COL_Y, COL_WIDTH, COL_HEIGHT};
//        Cursor cursor = db.query(button_table, columns, null, null,
//                null, null, null);

        Cursor c = db.rawQuery("SELECT * FROM " + command_table + " WHERE " + COL_COMMAND_ACTION_NAME + " = ?", new String[]{CommandName});

        int commandActionId = c.getColumnIndex(COL_ID);
        int buttonName = c.getColumnIndex(COL_BUTTON_NAME);
        int time = c.getColumnIndex(COL_TIME);
        int type = c.getColumnIndex(COL_TYPE);
        int pressure = c.getColumnIndex(COL_PRESSURE);
        int description = c.getColumnIndex(COL_DESCRIPTION);
        int header = c.getColumnIndex(COL_HEADER);
        int dataLength = c.getColumnIndex(COL_DATALENGTH);

        if (c.moveToFirst()) {
            CommandActionSet commandActionSet = new CommandActionSet();

            commandActionSet.id = c.getString(commandActionId);
            commandActionSet.detectedIcons = getButtonFromName(c.getString(buttonName));
            commandActionSet.actionTime = c.getString(time);
            commandActionSet.commandType = c.getInt(type);
            commandActionSet.pressure = c.getInt(pressure);
            commandActionSet.description = c.getString(description);
            commandActionSet.HEADER = c.getString(header);
            commandActionSet.dataLength = c.getString(dataLength);

            c.close();
            return commandActionSet;
        }


        c.close();
        return null;
    }

    public ArrayList<CommandActionSet> getCommandAction() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = new String[]{COL_ID, COL_BUTTON_NAME, COL_TIME, COL_TYPE, COL_PRESSURE, COL_DESCRIPTION,
                COL_HEADER, COL_DATALENGTH};
        Cursor cursor = db.query(commandAction_table, columns, null, null,
                null, null, null);
        String comment = "";

        ArrayList<CommandActionSet> commandActionSetArrayList = new ArrayList<>();
        int commandActionId = cursor.getColumnIndex(COL_ID);
        int buttonName = cursor.getColumnIndex(COL_BUTTON_NAME);
        int time = cursor.getColumnIndex(COL_TIME);
        int type = cursor.getColumnIndex(COL_TYPE);
        int pressure = cursor.getColumnIndex(COL_PRESSURE);
        int description = cursor.getColumnIndex(COL_DESCRIPTION);
        int header = cursor.getColumnIndex(COL_HEADER);
        int dataLength = cursor.getColumnIndex(COL_DATALENGTH);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

            CommandActionSet commandActionSet = new CommandActionSet();
            commandActionSet.id = cursor.getString(commandActionId);
            commandActionSet.detectedIcons = getButtonFromName(cursor.getString(buttonName));
            commandActionSet.actionTime = cursor.getString(time);
            commandActionSet.commandType = cursor.getInt(type);
            commandActionSet.pressure = cursor.getInt(pressure);
            commandActionSet.description = cursor.getString(description);
            commandActionSet.HEADER = cursor.getString(header);
            commandActionSet.dataLength = cursor.getString(dataLength);

            commandActionSetArrayList.add(commandActionSet);

        }
        cursor.close();
        return commandActionSetArrayList;
    }

    public ArrayList<String> getX() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = new String[]{COL_COMMAND_ID, COL_COMMAND_ACTION_ID};
        Cursor cursor = db.query(x_table, columns, null, null,
                null, null, null);

        ArrayList<String> x = new ArrayList();
        int iRowComment = cursor.getColumnIndex(COL_COMMAND_ID);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            x.add(cursor.getString(iRowComment));
        }
        cursor.close();
        return x;
    }

    public void close() {
        db.close();
    }

}
